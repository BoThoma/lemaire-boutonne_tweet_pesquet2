<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Take a snap from ISS</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css">
    <script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>

    <link rel="icon" href="https://pokoloruj.com.pl/static/gallery/promy-kosmiczne/okn3rze3.png">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/map.css">
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
</head>
<body>
    <div id="entete">

    <h1> Take a snap from ISS </h1>

    </div>


    <div id="app">
            <div id="Options_Global">
                <button class="icone_nav" @click="valideform">Localisez moi ! </button>
                <button class="icone_nav" @click="sat_traj">Trajectoire prévue </button>


                <div id="checkbox">
                    <input id="check" type="checkbox" v-model="check" @change="Click_boutton">
                    <label for="check">
                    <div id="check">
                    <span v-if="check">  Suivi de l'ISS en cours</span>
                    <span v-else>Suivi de l'ISS désactivé</span>
                    </div>
                </div>
                

                <div >
                Latitude: {{ latitude }}, Longitude: {{ longitude }},Zoom: {{ Zoom_sat }}
                </div>
            </div>

            <div id="map_photo_form">
                <label>
                    <img class="icone" src=https://cdn.pixabay.com/photo/2016/07/30/19/33/smartphone-1557796_1280.png >
                    <input type="radio" v-model="Zoom_sat" value="7"> Smartphone
                </label>
                <label>
                    <img class="icone" src=https://cdn.pixabay.com/photo/2017/03/19/00/39/camera-2155318_1280.png >
                    <input type="radio" v-model="Zoom_sat" value="10"> Reflex
                </label>
                <label>
                    <img class="icone" src=https://cdn-icons-png.flaticon.com/512/1224/1224937.png >
                    <input type="radio" v-model="Zoom_sat" value="13"> Téléobjectif
                </label>
                <button @click.prevent="part1">Prendre une photo</button>
            </div>


            <div id= consigne> <img class="icone_tweet" src="https://logodix.com/logo/14514.png"> Choisissez un niveau de zoom et créez votre commentaire comme si vous étiez dans l'espace ! </div>

            <div id="map_photo_text">
                <p>
                {{ Text_photo }}   ||   {{Text_photo_niv2}}
                </p>
            </div>
        
        <div id="polygone" style="position: absolute; top: 1100px; right: 11%; height: 400px; width: 33%; padding: 20px; background: #1f3047; border-radius: 20px; clip-path: polygon(20% 0, 100% 0, 80% 100%, 0% 100%)"></div>


        <div id="photo" style="position: absolute; top: 95%; left: 64% ;">
        <img :src="OrthophotoUrl" v-if="Orthobool" class="large-photo" width="360" height="auto"> 
        </div>

    </div>

    <div id="map"></div>

        
    <div id="footer">
        DM- Lemaire-Boutonné
    </div>

    <div id="another_footer">
        Données ISS temps-réel : NASA. Données carte : Leaflet | Tiles © Esri — Reverse-geocoding : GeoNames | Api trajectoire fourni par De Oliveira V. (ENSG)
    </div>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="/assets/map.js"></script>
</body>
</html>
