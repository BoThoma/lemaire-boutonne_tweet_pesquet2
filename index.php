<?php session_start();
require 'flight/Flight.php';

Flight::route('/', function () {
    echo Flight::render('map');
});

Flight::route('POST /api', function () {
    $url = Flight::request()->data['url'];
    print_r($url);

    $xml= simplexml_load_file($url);
    $json_data = json_decode(json_encode($xml), true);
    Flight::json($json_data);
});



Flight::start();
?>

