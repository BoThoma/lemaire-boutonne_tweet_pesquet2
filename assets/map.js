function project(lat, lng, zoom) {
    const R = 6378137;
    let sphericalScale = 0.5 / (Math.PI * R);
    let d = Math.PI / 180,
        max = 1 - 1E-15,
        sin = Math.max(Math.min(Math.sin(lat * d), max), -max),
        scale = 256 * Math.pow(2, zoom);

    let point = {
        x: R * lng * d,
        y: R * Math.log((1 + sin) / (1 - sin)) / 2
    };

    point.x = tiled(scale * (sphericalScale * point.x + 0.5));
    point.y = tiled(scale * (-sphericalScale * point.y + 0.5));

    return point;
}

function getTile(latlng, zoom) {
    let xy = project(latlng[0], latlng[1], zoom);
    return {
        x: xy.x,
        y: xy.y,
        z: zoom
    };
}

function tiled(num) {
    return Math.floor(num / 256);
}

// Récupération des coordonnées de tuile pour les coordonnées [48.85, 2.35] au zoom 15
let tileCoordinates = getTile([48.85, 2.35], 15);
console.log("Coordonnées de tuile pour Paris au zoom 15 :", tileCoordinates);




let map = L.map('map').setView([48.85, 2.34], 15); 
L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

let geo = L.geoJSON().addTo(map);

let api = 'http://api.open-notify.org/iss-now.json'
let api_traj = 'https://dev.iamvdo.me/orbit.php'

let mark;

var Icon = L.icon({
    iconUrl: "./assets/pngegg.png",
    iconSize: [26, 26], 
    iconAnchor: [13, 26],
    popupAnchor: [0, -26] 
});

Vue.createApp({
    data() {
        return {
            latitude: 48.85,
            longitude: 2.34,
            Zoom_sat: 15,
            OrthophotoUrl:"",
            Orthobool : false,
            Text_photo : "",
            check: false,
            niveauZoom: '',
            api_name:'',
            name_city: '',
            name_country: '',
            name_ocean: '',
            click_button: null,
            Text_photo_niv2 : "",
            donnees: '',
            lat_: '',
            long_: '',
        };
    },
    methods: {

        Click() {
            if (document.getElementById('check').checked) {
                this.check = true;
            }
        },

        Click_boutton() {
            this.click_button = document.getElementById('check');

            this.click_button.addEventListener('change',  () => {
            if (this.click_button.checked) {
                this.valideform();
                } 
            });

        },

        valideform () {
            fetch(api)
            .then(result => result.json())
            .then(result => { 
                this.latitude=result.iss_position.latitude;
                this.longitude=result.iss_position.longitude;

                if (mark && mark_label) {

                    map.removeLayer(mark)
                    map.removeLayer(mark_label)
                }
                mark=L.marker([this.latitude, this.longitude], {icon: Icon}).addTo(map);
                mark_label=L.marker([this.latitude, this.longitude],{ icon: L.divIcon({
                    className: 'description',   
                    html:  '<br><br>' + ' ' + ' Latitude:'+this.latitude+' Longitude:'+this.longitude  + '</br> '
                }), zIndexOffset: 5000 }).addTo(map);
                if(this.Zoom_sat ==  null)
                    map.setView([this.latitude, this.longitude], map.getZoom());
                else
                    map.setView([this.latitude, this.longitude], this.Zoom_sat);

                if (this.click_button && this.click_button.checked) {
                    setTimeout(this.valideform(), 5000);
                };
            });
        },



        sat_traj() {
            fetch(api_traj)
                .then(result_ => result_.json())
                .then(result_ => {
        
                    let tab_poly = [];
                    let tab_poly_2 = [];
                    let index_stop;
        
                    for (let i = 0; i < result_.length; i++) {
                        let lat = result_[i].lat;
                        let lng = result_[i].lng;
        

                        if (i > 0 && (result_[i].lng - result_[i - 1].lng) < -180) {
                            index_stop = i;
        

                            let polyligne = L.polyline(tab_poly);
                            polyligne.addTo(map);
        

                            tab_poly_2 = [];
        

                            let polyligne_2 = L.polyline(tab_poly_2);
                            polyligne_2.addTo(map);
                        } else {

                            if (index_stop) {
                                tab_poly_2.push([lat, lng]);
                            } else {
                                tab_poly.push([lat, lng]);
                            }
                        }
                    }
        

                    if (tab_poly.length > 0) {
                        let polyligne = L.polyline(tab_poly);
                        polyligne.addTo(map);
                    }
        
                    if (tab_poly_2.length > 0) {
                        let polyligne_2 = L.polyline(tab_poly_2);
                        polyligne_2.addTo(map);
                    }
                });
        },
        
        







        //Partie Formulaire (Niveau 1)

        part1(){
            this.OrthophotoUrlf();
            this.TextfromUrlf();
            this.lat_long();
        },

        OrthophotoUrlf() {
            let tileCoordinates2 = getTile([this.latitude, this.longitude], this.Zoom_sat);
            this.OrthophotoUrl = "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/"+this.Zoom_sat+"/"+tileCoordinates2.x+"/"+tileCoordinates2.y;
            this.Orthobool = true;
        },
        TextfromUrlf(){
            fetch("http://api.geonames.org/findNearbyPlaceNameJSON?lat="+this.latitude+"&lng="+this.longitude+"&username=bthomas")
                .then(reponseHttp => reponseHttp.json())
                .then(json => {
                    if(json.geonames && json.geonames.length > 0 && json.geonames[0].countryName != null ){
                        this.Text_photo = "Hello "+json.geonames[0].name+" #"+json.geonames[0].countryName;
                    }
                    else{
                        this.Text_photo = "Hello World";
                    }
                });
        },

        //Second niveau via un API Local (index.php)


        lat_long(){
            fetch(api)
            .then(result => result.json())
            .then(result => {
                this.lat_=result.iss_position.latitude;
                this.long_=result.iss_position.longitude;
                this.api_local();
                });
        },

        api_local() {
            let url = "http://api.geonames.org/extendedFindNearby?lat="+this.lat_+"&lng="+this.long_+"&username=LEMAIRELP";
            const data = new FormData()
            data.append('url', url)

            fetch('/api', {
                method: 'POST',
                body:data,
            })
            .then(response => response.json())
            .then(result => {
                console.log(result)
                if (result.ocean !== null || result.country !== null){
                    if (result.ocean !== undefined){
                        this.name_ocean=result.ocean.name;
                        let com_ocean = [["The "," seems calm from here "], ["The view is beautiful on the "," "], [" ", " is so blue !"], [" ", " seems peaceful from space"]];
                        let index_ocean = Math.round(Math.random() * com_ocean.length);
                        let val = com_ocean[index_ocean];
                        this.Text_photo_niv2 = val[0] + this.name_ocean + val[1];
                    }
                    else {
                        this.name_city=result.geoname[4].name;
                        this.name_country=result.geoname[2].countryName;
                        let com_city = [["The view of the "," is breathtaking"], ["The weather looks beautiful at "," "], ["From up there, life at ", " seeems to stop"], [" ", " seems peaceful from space"], [" The city of ", " seems very small"]];
                        let index_city = Math.round(Math.random() * com_city.length);
                        let val_2 = com_city[index_city];
                        this.Text_photo_niv2 = val_2[0] + this.name_city + val_2[1] + "#" + this.name_country;
                    }
                }else{
                    this.Text_photo_niv2 = "Hello World";
                }
            });
        }



    },
}).mount('#app');